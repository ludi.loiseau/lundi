Title: Home
Date: 2018/02
Slug: Home


## Typographie en recherche

Cette année l’option typographie met ses jeudis en commun pour redéfinir ce que peux être la/une recherche en typographie. Quelle serait la spécificité du dessin de caractères à l’erg? ☈ Suivant un premier ensemble de directions nous travaillerons en groupes mêlant bachelors et masters pour augmenter et délimiter les pistes de travail du quadrimestre et de la suite.

![ductus flok](/images/IMG_1264.jpg)


## Séminaire des 21, 22 et 23 février

Comme première étape dans la mise en action de cette recherche 3 journées typographiques avec le programme suivant :

#### Mercredi 21 matin
* conférence: [Paul Gangloff](http://writtenrecords.info/)
* conférence: Frederik Berlaen

#### Mercredi 21 après midi
* rencontre, discussion : Paul Gangloff
* workshop: Frederik Berlaen
* conférence: Luuse

#### Jeudi 22 matin    
* conférence: Baptiste Bernazeau 
* conférence: Julien Priez

#### Jeudi 22 après midi
* workshop: Frederik Berlaen
* workshop: Julien Priez

#### Vendredi 23 matin 
* conférence: Pierre Huyghebaert
* conférence: Johannes Verwoerd 

#### Vendredi 23 après-midi
* workshop: Frederik Berlaen
* workshop: Julien Priez
* workshop:  Johannes Verwoerd 
